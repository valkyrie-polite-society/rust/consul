#[cfg(test)]
mod tests {
  use crate::Consul;
  use crate::Result;
  use tokio::time::{sleep, Duration};

  #[tokio::test]
  async fn generate_and_restore_snapshot() -> Result<()> {
    let consul = Consul::new()?;
    let snapshot = consul.generate_snapshot().await?;
    assert_eq!(snapshot.is_empty(), false);

    sleep(Duration::from_secs(5)).await;

    let _ = consul.restore_snapshot(snapshot).await?;
    Ok(())
  }
}
