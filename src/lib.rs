mod tests;

use bytes::Bytes;
use reqwest::{Client, Certificate, Identity};
use reqwest::header::HeaderMap;
use reqwest::header::HeaderValue;
use thiserror::Error;

pub struct Consul {
  base: String,
  client: Client,
}

impl Consul {
  fn new() -> Result<Self> {
    Ok(Consul {
      base: "http://127.0.0.1:8500".into(),
      client: unauthenticated_client()?
    })
  }

  pub async fn generate_snapshot(&self) -> Result<Bytes> {
    let url = format!("{}/v1/snapshot", self.base);
    let response = self.client.get(url)
      .header("Accept", "application/x-gzip")
      .send()
      .await?;

    Ok(response.error_for_status()?.bytes().await?)
  }

  pub async fn restore_snapshot(&self, snapshot: Bytes) -> Result<()> {
    let url = format!("{}/v1/snapshot", self.base);
    let response = self.client.put(url)
      .body(snapshot)
      .header("Content-Type", "application/x-gzip")
      .send()
      .await?;

    let _ = response.error_for_status()?;
    Ok(())
  }
}

fn unauthenticated_client() -> Result<Client> {
  new_client(None, None, None)
}

fn new_client(
  token: Option<String>,
  root_ca: Option<Certificate>,
  identity: Option<Identity>
) -> Result<Client> {
  let mut headers = HeaderMap::new();
  headers.insert("Accept", HeaderValue::from_static("application/json"));

  if let Some(token) = token {
    if let Ok(value) = HeaderValue::from_str(&token) {
      headers.insert("X-Consul-Token", value);
    }
  }

  let mut client = Client::builder()
    .default_headers(headers);

  if let Some(root_ca) = root_ca {
    client = client.add_root_certificate(root_ca);
  }

  if let Some(identity) = identity {
    client = client.identity(identity);
  }

  Ok(client.build()?)
}

#[derive(Error, Debug)]
pub enum ConsulError {
  #[error("reqwest")]
  ReqwestError(#[from] reqwest::Error)
}

type Result<T> = std::result::Result<T, ConsulError>;
